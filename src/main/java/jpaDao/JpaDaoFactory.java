package jpaDao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import dao.DaoFactory;
import dao.FilmDao;
import dao.GenereDao;
import dao.UtenteDao;

public class JpaDaoFactory extends DaoFactory{
	
	public static EntityManager getManager() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("videoteca");
		EntityManager em = factory.createEntityManager();
		return em;
	}

	@Override
	public UtenteDao getUtenteDao() {
		return UtenteJpaDao.getInstance();
	}

	@Override
	public FilmDao getFilmDao() {
		return FilmJpaDao.getInstance();
	}

	@Override
	public GenereDao getGenereDao() {
		return GenereJpaDao.getInstance();
	}
}
