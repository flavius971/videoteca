package jpaDao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import dao.FilmDao;
import model.Film;

public class FilmJpaDao implements FilmDao{


	private static FilmJpaDao instance; 

	public static FilmDao getInstance() {
		if(instance == null) {
			instance = new FilmJpaDao();
		}
		return instance;
	}

	@Override
	public String aggiungiFilm(Film film) {
		EntityManager em = JpaDaoFactory.getManager();
		EntityTransaction et = em.getTransaction();
		try {
			et.begin();
			em.persist(film);
			et.commit();
			return "Nuovo Film aggiunto con successo";
		} catch (Exception e) {
			e.printStackTrace();
			return "Film gi� presente in catalogo";
		} finally {
			em.close();
		}
	}

	@Override
	public boolean rimuoviFilm(Film film) {
		EntityManager em = JpaDaoFactory.getManager();
		EntityTransaction et = em.getTransaction();
		try {
			et.begin();
			em.remove(em.merge(film));
			et.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			em.close();
		}
	}

	@Override
	public void modificaFilm(Film film) {
		EntityManager  em= JpaDaoFactory.getManager();
		EntityTransaction et= em.getTransaction();
		try {
			et.begin();
			em.merge(film);
			et.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}	finally {
			em.close();
		}

	}

	@Override
	public Film getFilm(int idFilm) {
		try {
			return (Film) JpaDaoFactory.getManager().find(Film.class,idFilm);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Film> getListaFilm() {
		Query q= JpaDaoFactory.getManager().createNamedQuery("Film.findAll",Film.class);
		return (List<Film>)q.getResultList();
	}

	@Override
	public List<Film> cercaFilm(String titolo) {
		String query = "SELECT f FROM Film f WHERE f.titolo=:titolo";
		Query q = JpaDaoFactory.getManager().createQuery(query);
		q.setParameter("titolo", titolo);
		List<Film> listaFilm=(List<Film>) q.getResultList();
		return listaFilm;
	}

}
