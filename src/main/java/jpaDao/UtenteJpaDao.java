package jpaDao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import dao.UtenteDao;
import model.Film;
import model.Utente;

public class UtenteJpaDao implements UtenteDao{

	private static UtenteJpaDao instance; 
	
	public static UtenteDao getInstance() {
		if(instance == null) {
			instance = new UtenteJpaDao();
		}
		return instance;
	}

	@Override
	public String aggiungiUtente(Utente u) {
		EntityManager em = JpaDaoFactory.getManager();
		EntityTransaction et = em.getTransaction();
		try {
			et.begin();
			em.persist(u);
			et.commit();
			return "Nuovo Utente aggiunto con successo";
		} catch (Exception e) {
			e.printStackTrace();
			return "Utente gi� registrato";
		} finally {
			em.close();
		}
	}

	@Override
	public boolean rimuoviUtente(Utente u) {
		EntityManager em = JpaDaoFactory.getManager();
		EntityTransaction et = em.getTransaction();
		try {
			et.begin();
			em.remove(em.merge(u));
			et.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			em.close();
		}
	}

	@Override
	public boolean modificaUtente(Utente u) {
		EntityManager  em= JpaDaoFactory.getManager();
		EntityTransaction et= em.getTransaction();
		try {
			et.begin();
			em.merge(u);
			et.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}	finally {
			em.close();
		}
	}

	@Override
	public Utente getUtente(String idUtente) {
		try {
			return (Utente) JpaDaoFactory.getManager().find(Utente.class,idUtente);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public Utente getUtenteByUsername(String username) {
		String query = "SELECT u FROM Utente u WHERE u.username=:username";
		Query q = JpaDaoFactory.getManager().createQuery(query);
		q.setParameter("username", username);
		try {
			return (Utente)q.getSingleResult();
		} catch (NoResultException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Utente login(String username, String password) {
		String query = "select u from Utente u where u.username=:username and u.password=:password";
		Query q = JpaDaoFactory.getManager().createQuery(query);
		q.setParameter("username", username);
		q.setParameter("password", password);
		try {
			return (Utente)q.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<Utente> getListaUtenti() {
		Query q= JpaDaoFactory.getManager().createNamedQuery("Utente.findAll",Utente.class);
		return (List<Utente>)q.getResultList();
	}

}
