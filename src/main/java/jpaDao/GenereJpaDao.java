package jpaDao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import dao.GenereDao;
import model.Film;
import model.Genere;

public class GenereJpaDao implements GenereDao{

	private static GenereJpaDao instance; 
	
	public static GenereDao getInstance() {
		if(instance == null) {
			instance = new GenereJpaDao();
		}
		return instance;
	}

	@Override
	public String aggiungiGenere(Genere g) {
		EntityManager em = JpaDaoFactory.getManager();
		EntityTransaction et = em.getTransaction();
		try {
			et.begin();
			em.persist(g);
			et.commit();
			return "Nuovo Genere aggiunto con successo";
		} catch (Exception e) {
			e.printStackTrace();
			return "Genere gi� presente";
		} finally {
			em.close();
		}
	}

	@Override
	public boolean rimuoviGenere(Genere g) {
		EntityManager em = JpaDaoFactory.getManager();
		EntityTransaction et = em.getTransaction();
		try {
			et.begin();
			em.remove(em.merge(g));
			et.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			em.close();
		}
	}

	@Override
	public Genere getGenere(int idGenere) {
		try {
			return (Genere) JpaDaoFactory.getManager().find(Genere.class,idGenere);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<Genere> getListaGeneri() {
		Query q= JpaDaoFactory.getManager().createNamedQuery("Genere.findAll",Genere.class);
		return (List<Genere>)q.getResultList();
	}

}
