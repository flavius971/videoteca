package filter;

import java.io.IOException;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Utente;




@WebFilter(filterName = "FiltroAdmin", urlPatterns = {"/AggiungiFilm", "/AggiungiGenere", "/GestioneUtenti", "/ModificaFilm", "/RimuoviFilm", "/RimuoviUtente"},dispatcherTypes = DispatcherType.REQUEST)
public class FiltroAdmin implements Filter {
	private FilterConfig filterConfig;

	@Override
	public void destroy() {
		this.filterConfig=null;
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		
		String admin = (String)request.getSession().getAttribute("admin");
		Utente u=(Utente)request.getSession().getAttribute("utente");
		
		if(admin!=null) {
			chain.doFilter(request, response);
		}else if(u!=null){
			response.sendRedirect("Home");
		}else if(u==null && admin==null) {
			response.sendRedirect("Login");
		}

	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;

	}

}