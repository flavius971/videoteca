package servlet.filtrate;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import model.JsonFilm;

@WebServlet("/Home")
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Home() {
		super();
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		ObjectMapper oggetto= new ObjectMapper();

		JsonNode json = oggetto.readTree(new URL("https://imdb-api.com/en/API/MostPopularMovies/k_x532r3xo")).get("items");
		
		// se su API non fosse stato array, potevo usare .readValue e creare direttamente l'oggetto JsomFilm
		
		List<JsonFilm> listaFilm= new ArrayList<>();

		int counter = 0;


		if(json.isArray()) {
			for(JsonNode j: json) {
				if(counter==5) {
					break;
				}
				JsonFilm jsonFilm= new JsonFilm(j.get("title").toString().replace("\"", ""), j.get("year").toString().replace("\"", ""), j.get("image").toString().replace("\"", ""), j.get("crew").toString().replace("\"", ""));
				listaFilm.add(jsonFilm);
				counter++;
			}
		}


		request.getSession().setAttribute("listaFilm", listaFilm);
		request.getRequestDispatcher("WEB-INF/jsp/jspPrivate/home.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
