package servlet.filtrate;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import model.Film;
import model.Genere;

/**
 * Servlet implementation class AggiungiFilm
 */
@WebServlet("/AggiungiFilm")
public class AggiungiFilm extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AggiungiFilm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("WEB-INF/jsp/jspPrivate/aggiungiFilm.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String titolo = request.getParameter("titolo");
		String regista = request.getParameter("regista");
		byte vietato = (byte)Integer.parseInt(request.getParameter("vietato"));		
		Genere genere = DaoFactory.getDaoFactory().getGenereDao().getGenere(Byte.parseByte(request.getParameter("genere")));
		String anno = request.getParameter("anno");
		String immagine = request.getParameter("immagine");
		
		Film f=new Film(titolo, regista, vietato, genere, anno, immagine);
		DaoFactory.getDaoFactory().getFilmDao().aggiungiFilm(f);
		request.getRequestDispatcher("WEB-INF/jsp/jspPrivate/catalogoFilm.jsp").forward(request, response);
	}

}
