package servlet.filtrate;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import model.Genere;

/**
 * Servlet implementation class AggiungiGenere
 */
@WebServlet("/AggiungiGenere")
public class AggiungiGenere extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AggiungiGenere() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("WEB-INF/jsp/jspPrivate/aggiungiGenere.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String genere= request.getParameter("genere");
		
		Genere g=new Genere(genere);
		DaoFactory.getDaoFactory().getGenereDao().aggiungiGenere(g);
		request.getRequestDispatcher("WEB-INF/jsp/jspPrivate/catalogoFilm.jsp").forward(request, response);
	}

}
