package servlet.filtrate;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import model.Film;
import model.Genere;

/**
 * Servlet implementation class ModificaFilm
 */
@WebServlet("/ModificaFilm")
public class ModificaFilm extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModificaFilm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id=Integer.parseInt(request.getParameter("idFilm"));
		request.setAttribute("idFilm", id);
		request.getRequestDispatcher("WEB-INF/jsp/jspPrivate/modificaFilm.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id=Integer.parseInt(request.getParameter("idFilm"));
		String titolo = request.getParameter("titolo");
		String regista = request.getParameter("regista");
		byte vietato = (byte)Integer.parseInt(request.getParameter("vietato"));	
		Genere genere = DaoFactory.getDaoFactory().getGenereDao().getGenere(Byte.parseByte(request.getParameter("genere")));
		String anno = request.getParameter("anno");
		String immagine = request.getParameter("immagine");
		
		Film f=new Film(id, titolo, regista, vietato, genere, anno, immagine);
		DaoFactory.getDaoFactory().getFilmDao().modificaFilm(f);
		response.sendRedirect("CatalogoFilm");
	}

}
