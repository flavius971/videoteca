package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import jpaDao.JpaDaoFactory;
import model.Utente;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor. 
	 */
	public Login() {
		// TODO Auto-generated constructor stub
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username=request.getParameter("username");
		String password=request.getParameter("password");


		if(username.equals("Admin") && password.equals("Admin")) {
			request.getSession().invalidate();
			request.getSession().setAttribute("admin", username);
			response.sendRedirect("Home");
		}else if(!username.equals("Admin") || !password.equals("Admin")) {
			Utente u = DaoFactory.getDaoFactory().getUtenteDao().login(username, password);
			if(u != null) {
				request.getSession().invalidate();
				request.getSession().setAttribute("utente", u);
				response.sendRedirect("Home");
			}
		}else {
			String messaggio = "Utente non esiste o i parametri inseriti sono errati, riprovare";
			request.setAttribute("messaggio", messaggio);
			request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);	
		}
	}

}
