package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import model.Utente;

/**
 * Servlet implementation class Registrazione
 */
@WebServlet("/Registrazione")
public class Registrazione extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public Registrazione() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("WEB-INF/jsp/registrazione.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String username = request.getParameter("username");
		String dataNascita = request.getParameter("dataNascita");
		Utente user = DaoFactory.getDaoFactory().getUtenteDao().getUtenteByUsername(username);
		
		if(user==null) {
			Utente u = new Utente(email, password, username, "utente", dataNascita);
			DaoFactory.getDaoFactory().getUtenteDao().aggiungiUtente(u);
			request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);	
		}
		else {
			String messaggio = "Utente gi� esistente o username gi� scelto";
			request.setAttribute("messaggio", messaggio);
			request.getRequestDispatcher("WEB-INF/jsp/registrazione.jsp").forward(request, response);
		}
		
	}

}
