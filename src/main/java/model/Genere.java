package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the genere database table.
 * 
 */
@Entity
@NamedQuery(name="Genere.findAll", query="SELECT g FROM Genere g")
public class Genere implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idGenere;

	private String genere;

	//bi-directional many-to-one association to Film
	@OneToMany(mappedBy="genere")
	private List<Film> films;

	public Genere() {
	}

	public Genere(String genere) {
		super();
		this.genere = genere;
	}

	public int getIdGenere() {
		return this.idGenere;
	}

	public void setIdGenere(int idGenere) {
		this.idGenere = idGenere;
	}

	public String getGenere() {
		return this.genere;
	}

	public void setGenere(String genere) {
		this.genere = genere;
	}

	public List<Film> getFilms() {
		return this.films;
	}

	public void setFilms(List<Film> films) {
		this.films = films;
	}

	public Film addFilm(Film film) {
		getFilms().add(film);
		film.setGenere(this);

		return film;
	}

	public Film removeFilm(Film film) {
		getFilms().remove(film);
		film.setGenere(null);

		return film;
	}

}