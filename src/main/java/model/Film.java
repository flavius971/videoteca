package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the film database table.
 * 
 */
@Entity
@NamedQuery(name="Film.findAll", query="SELECT f FROM Film f")
public class Film implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idFilm;
	
	private String anno;

	private String immagine;

	private String regista;

	private String titolo;

	private byte vm18;

	//bi-directional many-to-one association to Genere
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_Genere")
	private Genere genere;

	public Film() {
	}

	public Film(String titolo, String regista, byte vietato, Genere genere, String anno, String immagine) {
		super();
		this.anno = anno;
		this.immagine = immagine;
		this.regista = regista;
		this.titolo = titolo;
		this.vm18 = vietato;
		this.genere = genere;
	}

	public Film(int idFilm, String titolo, String regista, byte vm18, Genere genere, String anno, String immagine) {
		super();
		this.idFilm = idFilm;
		this.anno = anno;
		this.immagine = immagine;
		this.regista = regista;
		this.titolo = titolo;
		this.vm18 = vm18;
		this.genere = genere;
	}


	public int getIdFilm() {
		return idFilm;
	}

	public void setIdFilm(int idFilm) {
		this.idFilm = idFilm;
	}

	public String getAnno() {
		return anno;
	}

	public void setAnno(String anno) {
		this.anno = anno;
	}

	public String getImmagine() {
		return immagine;
	}

	public void setImmagine(String immagine) {
		this.immagine = immagine;
	}

	public String getRegista() {
		return regista;
	}

	public void setRegista(String regista) {
		this.regista = regista;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public byte getVm18() {
		return vm18;
	}

	public void setVm18(byte vm18) {
		this.vm18 = vm18;
	}

	public Genere getGenere() {
		return genere;
	}

	public void setGenere(Genere genere) {
		this.genere = genere;
	}


}