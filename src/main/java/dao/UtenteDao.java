package dao;

import java.util.List;

import model.Utente;

public interface UtenteDao {
	public abstract String aggiungiUtente(Utente u);
	public abstract boolean rimuoviUtente(Utente u);
	public abstract boolean modificaUtente(Utente u);
	public abstract Utente getUtente(String id);
	public abstract List<Utente> getListaUtenti();
	Utente getUtenteByUsername(String username);
	Utente login(String username, String password);
}
