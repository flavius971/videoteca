package dao;

import jpaDao.JpaDaoFactory;

public abstract class DaoFactory {
	
	public abstract UtenteDao getUtenteDao();
	public abstract FilmDao getFilmDao();
	public abstract GenereDao getGenereDao();
	
	public static DaoFactory getDaoFactory() {
		return new JpaDaoFactory();
	}
}
