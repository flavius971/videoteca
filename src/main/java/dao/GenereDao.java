package dao;

import java.util.List;

import model.Genere;

public interface GenereDao {
	public abstract String aggiungiGenere(Genere g);
	public abstract boolean rimuoviGenere(Genere g);
	public abstract Genere getGenere(int id);
	List<Genere> getListaGeneri();
}
