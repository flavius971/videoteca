package dao;

import java.util.List;

import model.Film;

public interface FilmDao {

	public abstract String aggiungiFilm(Film film);
	public abstract boolean rimuoviFilm(Film film);
	public abstract void modificaFilm(Film film);
	public abstract Film getFilm(int idFilm);
	public abstract List<Film> getListaFilm();
	public abstract List<Film> cercaFilm(String titolo);
}
