<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%
	String messaggioLogin = (String) request.getAttribute("messaggio");
	%>

	<% if (messaggioLogin != null){%>
	<script>
		alert("Utente gi� esistente o username gi� scelto");
	</script>
	<%} %>
	
	<form action="Registrazione" method="POST">
		<label for="email">Email</label> <br> 
		<input type="email" name="email" required><br><br>
		
		<label for="username">Username</label> <br> 
		<input type="text" name="username" required><br><br>
		
		<label for="password">Password</label><br> 
		<input type="password" name="password" required><br><br>
		
		<label for="data">Data di Nascita</label><br> 
		<input type="text" name="dataNascita" placeholder="0001-12-23" required><br><br>
		
		<button type="submit">Registrati</button>
	</form>
	
	<p>
		Sei gi� registrato? Accedi <a href="Login">qui</a>
	</p>
</body>
</html>