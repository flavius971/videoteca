<%@page import="java.util.List"%>
<%@page import="model.JsonFilm"%>
<%@page import="model.Utente"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<% String admin=(String)request.getSession().getAttribute("admin"); %>
<% List<JsonFilm> listaFilm= (List<JsonFilm>)request.getSession().getAttribute("listaFilm"); %>

	<a href="http://localhost:8080/videoteca/Home">Home</a>
	<a href="http://localhost:8080/videoteca/CatalogoFilm?">Catalogo Film</a> <br> <br>
	INSERIRE LA COSA DEL JSON <br> <br>
	
	<h1>Top 5 Film</h1>
	
	
      <table border=1>
          
              <tr>
                  <th>Titolo</th>
                  <th>Anno</th>
                  <th>Immagine</th>
                  <th>Crew</th>
                  
       		 </tr>
                	  
                  <%for(JsonFilm j: listaFilm){ %>
             <tr>
                  <th><%=j.getTitle() %></th>
                  <th><%=j.getYear() %></th>
                  <th><img src="<%=j.getImage()%>" height=150px width=150px></img></th>
                  <th><%=j.getCrew()%></th>
                  
       		 </tr>
                  <% }%>
             
          </table>
 
	
	<%if(admin!=null){ %>
	<form action="AggiungiFilm" method="GET">
		<input type="submit" value="AggiungiFilm">
	</form>
	<form action="AggiungiGenere" method="GET">
		<input type="submit" value="AggiungiGenere">
	</form>
	<form action="GestioneUtenti" method="GET">
		<input type="submit" value="GestioneUtenti">
	</form>
	<%} %>
	<form action="Logout" method="POST">
		<input type="submit" value="Logout">
	</form>
</body>
</html>