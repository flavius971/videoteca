<%@page import="java.time.LocalDate"%>
<%@page import="model.Utente"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Film"%>
<%@page import="java.util.List"%>
<%@page import="dao.DaoFactory"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	

	
	<a href="http://localhost:8080/videoteca/Home">Home</a>
	<a href="http://localhost:8080/videoteca/CatalogoFilm?">Catalogo Film</a> <br> <br>
	
	<% String admin=(String)request.getSession().getAttribute("admin"); %>
	<% List<Film> catalogoFilm=null;%>
	<% List<Film> filmNonVietati=new ArrayList(); %>
	<% Utente u=(Utente)request.getSession().getAttribute("utente"); %>
	
	<%try{
		catalogoFilm=DaoFactory.getDaoFactory().getFilmDao().getListaFilm();
		}catch(Exception e){
			e.printStackTrace();
		}
		%>


	<%if(admin!=null){
		if(catalogoFilm!=null){
			for(Film f:catalogoFilm){
				filmNonVietati.add(f);
			}	
		}
		
	}else if(u!=null){
		if(catalogoFilm!=null){
			for(Film f:catalogoFilm){
				if(f.getVm18()==0 || LocalDate.parse(u.getDataNascita()).isBefore(LocalDate.now().minusYears(18))){
				filmNonVietati.add(f);
				}
			}
		}
	
	}%>

	
      <h1>Lista Film</h1>
      <table border=1>
          
              <tr>
                  <th>Titolo</th>
                  <th>Genere</th>
                  <th>Regista</th>
                  <th>Anno</th>
                  <th>Immagine</th>
                  <%if(admin!=null){%>
                	  
                  
                  <th></th>
                  <th></th>
                  <% }%>
              </tr>
          
          
            <% if(filmNonVietati!=null){
            	for(Film f : filmNonVietati) {%>
           
			<tr>
				<td> <%= f.getTitolo()%> </td>
				<td> <%= f.getGenere().getGenere() %> </td>
				<td> <%= f.getRegista()%> </td>
				<td><%=f.getAnno()%></td>
				<td><img src="<%=f.getImmagine()%>" height=50px width=50px></img></td>
				 <%if(admin!=null){%>
				<td>
					<form action="RimuoviFilm" method = "post">
						<input type = "hidden" name = "idFilm" value = "<%= f.getIdFilm() %>"> 
						<input type = "submit" value = "Rimuovi">
					</form>
				</td>
				<td>
					<form action="ModificaFilm" method = "get">
						<input type = "hidden" name = "idFilm" value = "<%= f.getIdFilm() %>"> 
						<input type = "submit" value = "Modifica">
					</form>
				</td>
				<%} %>
			</tr>
		<%}
        }%>
          
      </table>
      <br> <br>
      <%if(admin!=null){ %>

	<form action="AggiungiFilm" method="GET">
		<input type="submit" value="AggiungiFilm">
	</form>
	<form action="AggiungiGenere" method="GET">
		<input type="submit" value="AggiungiGenere">
	</form>
	<form action="GestioneUtenti" method="GET">
		<input type="submit" value="GestioneUtenti">
	</form>
	<%} %>
	<form action="Logout" method="POST">
		<input type="submit" value="Logout">
	</form>
</body>
</html>