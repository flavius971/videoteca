<%@page import="dao.DaoFactory"%>
<%@page import="model.Film"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<%int id=(int)request.getAttribute("idFilm"); %>
	<% Film f= DaoFactory.getDaoFactory().getFilmDao().getFilm(id); %>

	<a href="http://localhost:8080/videoteca/Home">Home</a>
	<a href="http://localhost:8080/videoteca/CatalogoFilm?">Catalogo Film</a> <br> <br>
	
<form action="ModificaFilm" method="POST">
		
		<label for="titolo">Titolo</label> <br> 
		<input type="text" name="titolo" value = "<%= f.getTitolo() %>" required><br><br>
		
		<label for="regista">Regista</label> <br> 
		<input type="text" name="regista" value = "<%= f.getRegista()%>" required><br><br>
		
		<label for="vietato">Vietato ai Minori: 0=no, 1=si</label><br> 
		<input type="number" name="vietato" value = "<%= f.getVm18() %>" required><br><br>
		
		<label for="genere">Genere</label><br> 
		<input type="number" name="genere" value = "<%= f.getGenere().getIdGenere()%>" required><br><br>
		
		<label for="anno">Anno di Uscita</label><br> 
		<input type="text" name="anno" value = "<%= f.getAnno() %>" placeholder="0001/12/23" required><br><br>
		
		
		<label for="immagine">Immagine</label><br> 
		<input type="text" name="immagine" value = "<%= f.getImmagine() %>" required><br><br>
		
		<input type = "hidden" name = "idFilm" value = "<%= f.getIdFilm() %>">
		<button type="submit">Modifica</button>
	</form>
</body>
</html>