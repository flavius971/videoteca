<%@page import="dao.DaoFactory"%>
<%@page import="model.Utente"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<a href="http://localhost:8080/videoteca/Home">Home</a>
	<a href="http://localhost:8080/videoteca/CatalogoFilm?">Catalogo Film</a> <br> <br>
	
	<%List<Utente> listaUtenti = DaoFactory.getDaoFactory().getUtenteDao().getListaUtenti(); %>
	 
	 
      <h1>Lista Utenti</h1>
      <table border=1>
          
              <tr>
                  <th>Email</th>
                  <th>Username</th>
                  <th>Data di Nascita</th>
                  <th>Ruolo</th>
                  <th></th>
              </tr>
          
          
            <% for(Utente u : listaUtenti) {%>
			<tr>
				<td> <%= u.getEmail()%> </td>
				<td> <%= u.getUsername() %> </td>
				<td> <%= u.getDataNascita()%> </td>
				<td><%=u.getRuolo()%></td>
				<td>
					<form action="RimuoviUtente" method = "post">
						<input type = "hidden" name = "username" value = "<%= u.getUsername() %>"> 
						<input type = "submit" value = "Rimuovi">
					</form>
				</td>
			</tr>
		<%} %>
          
      </table>
      <br> <br>
    
	<form action="AggiungiFilm" method="GET">
		<input type="submit" value="AggiungiFilm">
	</form>
	<form action="AggiungiGenere" method="GET">
		<input type="submit" value="AggiungiGenere">
	</form>
	<form action="GestioneUtenti" method="GET">
		<input type="submit" value="GestioneUtenti">
	</form>
	<form action="Logout" method="POST">
		<input type="submit" value="Logout">
	</form>
</body>
</html>