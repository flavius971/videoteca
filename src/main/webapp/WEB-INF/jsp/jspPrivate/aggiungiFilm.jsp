<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<a href="http://localhost:8080/videoteca/Home">Home</a>
	<a href="http://localhost:8080/videoteca/CatalogoFilm?">Catalogo Film</a> <br> <br>
	
	<form action="AggiungiFilm" method="POST">
		<label for="titolo">Titolo</label> <br> 
		<input type="text" name="titolo" required><br><br>
		
		<label for="regista">Regista</label> <br> 
		<input type="text" name="regista" required><br><br>
		
		<label for="vietato">Vietato ai Minori: 0=no, 1=si</label><br> 
		<input type="number" name="vietato" required><br><br>
		
		<label for="genere">Genere</label><br> 
		<input type="number" name="genere" required><br><br>
		
		<label for="anno">Anno di Uscita</label><br> 
		<input type="text" name="anno" placeholder="0001/12/23" required><br><br>
		
		
		<label for="immagine">Immagine</label><br> 
		<input type="text" name="immagine" required><br><br>
		
		<button type="submit">Inserisci</button>
	</form>
</body>
</html>